package reseau_neuronal;
import java.util.*;

public class Couche {
	int niveau;
	neurone[] neurones;
	int nbneurones;
	Couche(int nbneurones, int niveau,int nbentree)
	{
		this.nbneurones=nbneurones;
		this.niveau=niveau;
		this.neurones=new neurone[nbneurones];
		for (int i=0;i<this.nbneurones;i++)
		{
			neurones[i]=new neurone(nbentree);
		}
		
	}
	void Setentree(ArrayList<Double> Donnees) {
		for (int i=0;i<this.nbneurones;i++)
		{
			neurones[i].Set_entree(Donnees);
		}
		
	}
	ArrayList<Double> Activation() {
		ArrayList<Double> sorties=new ArrayList<Double>();
		
		for (int i=0;i<this.nbneurones;i++)
		{
			sorties.add(this.neurones[i].Activation());
		}
		return sorties;
		
		
	}
}
