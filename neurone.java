package reseau_neuronal;
import java.util.*;
import java.lang.Math;

public class neurone {
	
	private int nb_entree;
	private Double[] entrees =new Double[4] ;
	private Double[] poids = new Double[4] ;

	neurone(int nbentree)
	{
		this.nb_entree=nbentree;
		for (int i=0;i<4;i++)
		{
		this.entrees[i] = 0.;
		this.poids[i]=0.1;
		}
	}
	void Set_entree(ArrayList<Double> entrees){
		for (int i=0;i<this.nb_entree;i++) 
		{
			
			this.entrees[i]=entrees.get(i);
		}
	}
	 Double[] Get_entree(){		
		return this.entrees;
	}
	 Double[] Get_poids(){		
		return this.poids;
	}
	 int  Get_nbentree(){		
		return this.nb_entree;
	}
	 void Set_poids(Double[] poids) {
			for (int i=0;i<this.nb_entree;i++) 
			{
				
				this.poids[i]=poids[i];
			}
	 }
	Double Activation() {
		Double somme=0.0;
		for (int i=0;i<nb_entree;i++) 
		{
			somme=somme+entrees[i]*poids[i];
			
		}
		Double res=1/(1+Math.exp(-somme));
		return res;
		
	}
	

}
