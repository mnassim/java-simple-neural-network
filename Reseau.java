package reseau_neuronal;
import java.util.*;

public class Reseau {
	
	ArrayList<Couche> reseau = new ArrayList<Couche>();
	Double taux;
	ArrayList<Double> Donnees = new ArrayList<Double>();
	int nbcouches;
	
	Reseau(){
		this.taux=.01;
		this.nbcouches=3;
		
			this.reseau.add(new Couche(4,1,4));
			this.reseau.add(new Couche(2,2,4));
			this.reseau.add(new Couche(1,3,2));
				
	}
	void SetDonnees(ArrayList<Double> Donnees) {
	this.Donnees=Donnees;
	
	
}
		
		
	
	void Setentree(int couche,ArrayList<Double> entree) {
		reseau.get(couche).Setentree(entree);	
	}
	Double Activation()
	{

		ArrayList<Double> nvlleentree= new ArrayList<Double>();
		this.reseau.get(0).Setentree(Donnees);
		nvlleentree=this.reseau.get(0).Activation();
		this.reseau.get(1).Setentree(nvlleentree);
		nvlleentree=this.reseau.get(1).Activation();
		this.reseau.get(2).Setentree(nvlleentree);
		nvlleentree=this.reseau.get(2).Activation();
		
		return nvlleentree.get(0);
		
	}
	void Correction(Double erreur)
	{
		//couche de sortie
		Couche sortie=this.reseau.get(2);
		Double[] entrees=sortie.neurones[0].Get_entree();
		Double[] poids=sortie.neurones[0].Get_poids();
		Double[] nvxpoids= new Double[2];
		Double somme=0.;
		for(int i=0;i<sortie.neurones[0].Get_nbentree();i++)
		{
			somme=somme+entrees[i]*poids[i];
		}
		for(int i=0;i<sortie.neurones[0].Get_nbentree();i++)
		{
			nvxpoids[i]=poids[i]+(this.taux*erreur*(Math.exp(-somme)/( Math.pow(2,1+Math.exp(-somme))))*entrees[i]);
		}
		this.reseau.get(2).neurones[0].Set_poids(nvxpoids);
		
		//couche intermediaire 1
		//calcul de l'erreur pour chaque neurone de la couche
		Couche int1 =this.reseau.get(1);
		Double[] erreurint1 =new Double[2];//erreures des neurones de la premiere couche intermediaire
		
		Double[] entrees1;
		Double[] poids1;
		Double[][] nvxpoids1= new Double[2][4];
		somme=0.;
	    for (int i = 0;i<2;i++)
	    {
			entrees1=int1.neurones[i].Get_entree();
			 poids1=int1.neurones[i].Get_poids();
			 for(int j=0;j<4;j++) somme=somme+entrees1[j]*poids1[j];
			 erreurint1[i]=sortie.neurones[0].Get_poids()[i]*erreur*(Math.exp(-somme)/( Math.pow(2,1+Math.exp(-somme))));
			 for(int j=0;j<4;j++)
			 nvxpoids1[i][j]=poids1[j]+erreurint1[i]*entrees1[j]*this.taux;
		
	    }
	    this.reseau.get(1).neurones[0].Set_poids(nvxpoids1[0]);
	    this.reseau.get(1).neurones[1].Set_poids(nvxpoids1[1]);
		//couche intermediaire 2
		//calcul de l'erreur pour chaque neurone de la couche
		Couche int2 =this.reseau.get(0);
		Double[] erreurint2 =new Double[4];//erreures des neurones de la premiere couche intermediaire
		Double[] entrees2;
		Double[] poids2;
		Double[][] nvxpoids2= new Double[4][4];
		somme=0.;
	    for (int i = 0;i<4;i++)
	    {
			entrees2=int2.neurones[i].Get_entree();
			 poids2=int2.neurones[i].Get_poids();
			 for(int j=0;j<4;j++) somme=somme+entrees2[j]*poids2[j];
			 erreurint2[i]=int1.neurones[0].Get_poids()[i]*(erreurint1[0]+erreurint1[1])*(Math.exp(-somme)/( Math.pow(2,1+Math.exp(-somme))));
			 for(int j=0;j<4;j++)
			 nvxpoids2[i][j]=poids2[j]+erreurint2[i]*entrees2[j]*this.taux;
		
	    }
	    this.reseau.get(0).neurones[0].Set_poids(nvxpoids2[0]);
	    this.reseau.get(0).neurones[1].Set_poids(nvxpoids2[1]);
	    this.reseau.get(0).neurones[2].Set_poids(nvxpoids2[2]);
	    this.reseau.get(0).neurones[3].Set_poids(nvxpoids2[3]);
		
	}
	
	
	

}
