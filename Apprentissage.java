package reseau_neuronal;

import java.util.ArrayList;
import java.util.Scanner;

public class Apprentissage {
	private Reseau reseau;

	private ArrayList<Double[]> examples;
	
	
	Apprentissage(){
		
		this.reseau= new Reseau();
		this.examples=new ArrayList<Double[]>();
	}

	
	 public void setrawdata(java.io.File arg)  throws java.io.IOException {
			System.out.println("chargement des donn�es");
			java.util.Scanner lecteur ;
			
			java.io.File fichier = arg;
			lecteur = new java.util.Scanner(fichier);
			String ligne;
			String[] tab= new String[5];;
			Double[] tabd = new Double[5];
			while(lecteur.hasNext())
			{
				tab=new String[5];
				tabd = new Double[5];
				ligne=lecteur.next();
				System.out.println(ligne);
			tab=ligne.split(",");
			for(int i=0;i<tab.length;i++)
			{
				
				if (i==4) {
					if (tab[i].equals("Iris-virginica")) tabd[i]=(0.165);
					else if (tab[i].equals("Iris-versicolor")) tabd[i]=(0.495);
					else if (tab[i].equals("Iris-setosa")) tabd[i]=(0.825);
				}
				else 
				tabd[i]=(Double.valueOf(tab[i]));
				
			}
			this.examples.add(tabd);
			}
	 }
	public void startapprentissage()//apprentissage stochastique
	{
	
		ArrayList<Double> entree = new ArrayList<Double>();
	
		Double resultat;
		Double erreur;

		int tour=0;
		boolean fini=false;
		int count;
		System.out.println("nous effectuons l'apprentissage sur   "+this.examples.size()+"exemples");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        while (!fini)
        {tour++;
        	
        
		for (Double[] tab : examples)
		{	
			erreur=1.;
			while(Math.abs(erreur)>0.17)
			{
		
			entree.clear();
			for (int i=0;i<4;i++)
				entree.add(tab[i]/10);//on divise les entrees par 10 afin qu'elles soient comprises en 0 et 1
			this.reseau.SetDonnees(entree);

			resultat=this.reseau.Activation();
			erreur=(tab[4]-resultat);
				
			if (Math.abs(erreur)>0.17) {
			this.reseau.Correction(erreur);
			tour++;
			}
			}
		}
	count=0;
		for (Double[] tab : examples)
		{	

			entree.clear();
			for (int i=0;i<4;i++)
			entree.add(tab[i]/10);
			this.reseau.SetDonnees(entree);

			resultat=this.reseau.Activation();
			erreur=(tab[4]-resultat);
			if (Math.abs(erreur)>0.17) count++;
		
			}
		if (count>0) System.out.println("il reste  "+count+"  erreures a corriger");
		else {
			fini=true;
			System.out.println("l'apprentissage a dur� "+tour+"tours");
		}
		}
			
		}
		

	
	public static void main(String[] args) {
		//Scanner sc = new Scanner(System.in);
		Apprentissage ap =new Apprentissage();
		java.io.File fichier = new java.io.File(args[0]);
		java.io.File fichiervalidation = new java.io.File(args[1]);
		ArrayList<Double> entree=new ArrayList<Double>();
		Double resultat;
		//String str;
	//	String[] tab ;
	//	Double[] tabd;
		try {ap.setrawdata(fichier);}
		catch(java.io.IOException e) {
			System.out.println("erreur de lecture du fichier !");
			}
		ap.startapprentissage();
		System.out.println("apprentissage fini");
		 //validation manuelle
		/*boolean test =true;  
				while (test)
				{
		System.out.println("voulez vous tester votre apprentissage???");
		str = sc.nextLine();
		 tab = str.split(",");
	    tabd =new Double[4];
		for(int i=0;i<tab.length;i++)
		{
			
			tabd[i]=(Double.valueOf(tab[i]));
			
		}
	
		for (int i=0;i<4;i++)
			entree.add(tabd[i]/10);
		ap.reseau.SetDonnees(entree);
		resultat=ap.reseau.Activation();
		if(resultat<0.33) System.out.println("c'est une Iris-virginica "+resultat);
		else if(resultat<0.66 && resultat>0.33 ) System.out.println("Iris-versicolor "+resultat);
		else  System.out.println("Iris-setosa "+resultat);
		entree.clear();
		
				}*/
		
		//validation
		ap.examples.clear();
		try {ap.setrawdata(fichiervalidation);}
		catch(java.io.IOException e) {
			System.out.println("erreur de lecture du fichier de validation !");
			}
		System.out.println("nous effectuons les validations sur   "+ap.examples.size()+"exemples");
		Double erreur;
		int count=0;
		int nberreur=0;
		
		for (Double[] tab1 : ap.examples)
		{	
			count++;
			entree.clear();
			for (int i=0;i<4;i++)
			entree.add(tab1[i]/10);
			ap.reseau.SetDonnees(entree);

			resultat=ap.reseau.Activation();
			erreur=(tab1[4]-resultat);
			if (Math.abs(erreur)>0.165) nberreur++;
			System.out.println("test : "+count+" erreure :"+erreur);//si l'erreur est inferieur a 0.165 le reseau se trompe lors de la classification
			
	}
		System.out.println("nb erreurs: "+nberreur);
	}
	

}
